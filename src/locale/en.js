const en = {
  upvote: 'Upvote',
  answers: 'Answers',
  comments: 'Comments',
  reply: 'Reply',
  submit: 'Submit',
  submit2: 'Submit',
  login: 'Log In',
  reset: 'Reset',
  'email.placeholder': 'Your Mail',
  'password.placeholder': 'Your Password',
  'view.more': 'View More',
  'view.all.answer': 'View All Answers of this Question',
  'replies.all': 'View All Replies',
  'replied.to': 'replied to',
  'navigator.post': 'Add Qustion',
  'navigator.profile': 'View Profile',
  'navigator.admin': 'Go to Admin Page',
  'navigator.logout': 'Log Out',
  'write.answer': 'Add Answer',
  'latest.questions': 'New Questions',
  'post.question': 'Add Qustion',
  'post.question.placeholder': 'Start your question here.',
  'post.answer': 'Add Answer',
  'post.comment.placeholder': 'Add a Comment here...',
  'editor.placeholder.question': 'Description of your question (option)',
  'editor.placeholder.answer': 'Write your Answer here...',
  'editor.enter': 'Add',
  'editor.enter.placeholder': 'Enter URL',
  'index.post.question': 'What is your question?',
  'list.answer.at': 'Answered at',
  'list.answer.more': 'View More Answers',
  'list.comment.more': 'View More Comments',
  'list.reply.more': 'View More Replies',
  'profile.tab.answers': 'Answers',
  'profile.tab.questions': 'Questions',
  'profile.tab.documents': 'Documents',
  'admin.banner': 'Administrator Dashboard',
  'admin.table': 'Tables',
  'admin.query': 'ID Query',
  'admin.pagination': 'Page',
  'admin.create.user': 'Create User',
  'admin.if.delete': 'Do you want to delete it?',
  'admin.delete.it': 'Delete it',
  nodata: 'No Data',
  'nodata.answer': 'No Answers Yet',
  'nodata.document': 'No Documents Yet',
  'nodata.question': 'No Questions Yet',
  error: 'error',
  'contact.admin': 'Please contact the Administrator.'
};

export default en;
