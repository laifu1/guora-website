import RequestService from 'service/RequestServices/base';

const login = data => {
  return RequestService.Post(`/api/web/security/login`, data);
};

export default { login };
