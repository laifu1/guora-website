import RequestService from 'service/RequestServices/base';

const logout = () => {
  return RequestService.Post(`/api/web/security/logout`);
};

export default { logout };
