import RequestService from 'service/RequestServices/base';

const fetchComments = (id, limit, offset) => {
  return RequestService.Get(`/api/web/answer/${id}/comments?offset=${offset}&limit=${limit}`);
};

const postComment = ({ answerID, content, type }) => {
  return RequestService.Post(`/api/web/comment`, { answerID, content, type });
};

const postCommentSupporter = id => {
  return RequestService.Post(`/api/web/comment/${id}/supporters`, {});
};

const deleteCommentSupporter = id => {
  return RequestService.Delete(`/api/web/comment/${id}/supporters`);
};

export default {
  fetchComments,
  postComment,
  postCommentSupporter,
  deleteCommentSupporter
};
