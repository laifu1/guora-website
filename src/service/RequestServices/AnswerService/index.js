import RequestService from 'service/RequestServices/base';

const postAnswer = ({ questionID, content, type }) => {
  return RequestService.Post(`/api/web/answer`, { questionID, content, type });
};

const postAnswerSupporter = id => {
  return RequestService.Post(`/api/web/answer/${id}/supporters`, {});
};

const deleteAnswerSupporter = id => {
  return RequestService.Delete(`/api/web/answer/${id}/supporters`);
};

export default {
  postAnswer,
  postAnswerSupporter,
  deleteAnswerSupporter
};
