const timeout = time => {
  return new Promise(resolve => {
    setTimeout(() => {
      resolve(0);
    }, time);
  });
};

const BufferMethod = func => (...args) =>
  Promise.all([func.apply(this, args), timeout(800)]).then(([funcResponse]) => {
    return Promise.resolve(funcResponse);
  });

export default BufferMethod;

/**
 * @ Debug
 * @ Debug
 * @ Debug
 *

   const c = BufferMethod(Math.max);

   c(1,2,3,4,5,9,8).then(max => console.log(max)); // -> 9

 *
 */
