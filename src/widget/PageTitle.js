import styled from 'styled-components';
import { Black } from 'style/Color';
import { Bold } from 'style/Weight';

const PageTitle = styled.h1`
  font-weight: ${Bold};
  font-size: 24px;
  line-height: 1.1;
  color: ${Black};
`;

export default PageTitle;
