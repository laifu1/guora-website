import styled from 'styled-components';

const ContainerWithBottomLine = styled.section`
  position: relative;
  ::after {
    position: absolute;
    left: 0;
    right: 0;
    bottom: 0;
    margin: 0 ${props => props.margin || 0}px;
    display: ${props => (props.bottomLine ? 'block' : 'none')};
    border-bottom: 1px solid #e2e2e2;
    content: '';
  }
`;
export default ContainerWithBottomLine;
