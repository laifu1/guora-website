import React, { Component } from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';
import TextArea from 'widget/TextArea';
import AutoScale from 'widget/AutoScale';
import MeService from 'service/MeService';
import { White, GrayLight3, GrayLight4, BrightBlue, Blue, HoverBlue } from 'style/Color';
import { Bold } from 'style/Weight';

const TextCardRoot = styled.div`
  padding-bottom: 10px;
`;
const TextCardWrap = styled.div`
  display: flex;
`;

const Circle = styled.div`
  border-radius: 50%;
  overflow: hidden;
  width: 32px;
  height: 32px;
`;

const Bar = styled.div`
  margin-left: 12px;
  display: flex;
  flex: 1;
`;

const Radius = styled.div`
  border: 1px solid ${props => (props.focus ? GrayLight3 : GrayLight4)};
  background-color: ${White};
  vertical-align: middle;
  border-radius: 16px;
  padding: 7px 16px;
  display: flex;
  flex: 1;
`;

const ReplyTextArea = styled(TextArea)`
  line-height: 16px;
  font-size: 12px;
  padding: 0;
  flex: 1;
`;

const ScaleButton = styled(AutoScale)`
  height: 32px;
  color: ${White};
  font-size: 13px;
  font-height: ${Bold};
  line-height: 20px;
  padding: 6px 15px;
  border-radius: 16px;
  box-sizing: border-box;
  cursor: ${props => (props.disabled ? 'default' : 'pointer')};
  background-color: ${props => (props.disabled ? BrightBlue : Blue)};
  ${props =>
    !props.disabled &&
    `
    :hover { 
        background-color: ${HoverBlue};
    }
  `};
`;

const Submit = styled(ScaleButton)`
  margin-left: 8px;
`;

class TextCard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      content: '',
      focus: false
    };
    this.inputRef = React.createRef();
    this.handleFocus = this.handleFocus.bind(this);
    this.handleBlur = this.handleBlur.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.submit = this.submit.bind(this);
  }

  componentDidMount() {
    const { autofocus } = this.props;
    if (autofocus) {
      this.inputRef.current.focus();
    }
  }

  handleFocus() {
    let { focus } = this.state;
    focus = true;
    this.setState({ focus });
    this.props.onFocus();
  }

  handleBlur() {
    let { focus } = this.state;
    const { content } = this.state;

    focus = false;
    this.setState({ focus });
    if (content.length === 0) {
      this.props.onBlur();
    }
  }

  handleChange(event) {
    this.setState({ content: event.target.value });
  }

  submit() {
    const { content } = this.state;
    let { popup } = this.state;
    const disabled = content.length === 0;
    if (disabled) {
      return;
    }
    this.props.onSubmit(content);
    popup = true;
    this.setState({ popup });
  }

  render() {
    const { placeholder } = this.props;
    const { focus, content } = this.state;
    const disabled = content.length === 0;
    return (
      <TextCardRoot>
        <TextCardWrap>
          <Circle>{MeService.AvatarComponent()}</Circle>
          <Bar>
            <Radius focus={focus}>
              <ReplyTextArea
                onFocus={this.handleFocus}
                placeholder={placeholder}
                ref={this.inputRef}
                onChange={this.handleChange}
                onBlur={this.handleBlur}
              />
            </Radius>
            {(!disabled || focus) && (
              <Submit role="button" onClick={this.submit} disabled={disabled}>
                <FormattedMessage id="submit" />
              </Submit>
            )}
          </Bar>
        </TextCardWrap>
      </TextCardRoot>
    );
  }
}

TextCard.defaultProps = {
  autofocus: false,
  onFocus: () => {},
  onBlur: () => {}
};

TextCard.propTypes = {
  onSubmit: PropTypes.func.isRequired,
  placeholder: PropTypes.string.isRequired,
  autofocus: PropTypes.bool,
  onFocus: PropTypes.func,
  onBlur: PropTypes.func
};

export default TextCard;
