import styled from 'styled-components';
import { Blue, Black } from 'style/Color';
import { Bold } from 'style/Weight';

const Link = styled.a`
  text-decoration: none;
  line-height: 20px;
  font-weight: ${Bold};
  font-size: 15px;
  color: ${Black};
  margin: 0;
  :hover {
    color: ${Blue};
  }
`;

const MediumLinkProfile = Link;

export default MediumLinkProfile;
