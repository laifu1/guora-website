import styled from 'styled-components';
import { Blue, Black } from 'style/Color';
import { Bold } from 'style/Weight';

const Link = styled.a`
  text-decoration: none;
  line-height: 16px;
  font-weight: ${Bold};
  font-size: 13px;
  color: ${Black};
  margin: 0;
  :hover {
    color: ${Blue};
  }
`;

const SmallLinkProfile = Link;

export default SmallLinkProfile;
