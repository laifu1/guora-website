import React, { Component } from 'react';
import QuestionService from 'service/RequestServices/QuestionService';
import { URL } from 'service/URLService';
import MainNavigator from 'component/Navigators/MainNavigator';
import AskModal from 'component/Modals/AskModal';

class BasePageCombination extends Component {
  constructor(props) {
    super(props);
    this.state = {
      modal: false
    };

    this.toggle = () => {
      let { modal } = this.state;
      modal = !modal;
      this.setState({ modal });
    };

    this.ask = data => {
      const { title, desc } = data;
      QuestionService.postQuestion({ title, desc }).then(({ record, ra }) => {
        if (ra === 1) {
          URL.go(URL.question(record.id));
        }
      });
    };
  }

  render() {
    const { modal } = this.state;
    return [
      <MainNavigator key="nav" onClickAsk={this.toggle} />,
      modal && <AskModal key="modal" onClose={this.toggle} onSubmit={this.ask} />
    ];
  }
}

export default BasePageCombination;
