/* eslint-disable react/prefer-stateless-function */
import React, { Component } from 'react';
import styled from 'styled-components';
import { FormattedMessage } from 'react-intl';
import EmptyPage from 'widget/EmptyPage';

const Root = styled.div`
  position: relative;
`;

class DocumentList extends Component {
  render() {
    const { ...props } = this.props;
    return (
      <Root {...props} style={{ minHeight: 180 }}>
        <EmptyPage>
          <FormattedMessage id="nodata.document" />
        </EmptyPage>
      </Root>
    );
  }
}

export default DocumentList;
