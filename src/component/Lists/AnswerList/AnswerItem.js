import React, { Component } from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import LinkTitle from 'component/LinkTitle';
import MediumProfileBar from 'component/ProfileBars/MediumProfileBar';
import RichTextCollapse from 'component/RichTexts/RichTextCollapse';
import AnswerSocialBar from 'component/SocialBars/AnswerSocialBar';

const AnswerItemRoot = styled.div``;

const Container = styled.div`
  margin-top: 8px;
`;

class AnswerItem extends Component {
  constructor(props) {
    super(props);
    this.state = {
      injected: false,
      display: true
    };
    this.inject = this.inject.bind(this);
  }

  inject() {
    let { injected, display } = this.state;
    if (injected) {
      display = !display;
      this.setState({ display });
      return;
    }
    injected = true;
    this.setState({ injected });
  }

  render() {
    const { data, expand, injectComponent } = this.props;
    const { injected, display } = this.state;

    return (
      <AnswerItemRoot>
        {data.title ? (
          [
            <LinkTitle key={'title'} href={data.link}>
              {data.title}
            </LinkTitle>,
            <Container key={'container'}>
              <MediumProfileBar data={data.profile} />
            </Container>
          ]
        ) : (
          <MediumProfileBar data={data.profile} />
        )}
        <RichTextCollapse data={data.content} expand={expand} />
        <AnswerSocialBar data={data.social} id={data.id} onClickComment={this.inject} />
        {injected && injectComponent({ style: { display: display ? 'block' : 'none' } })}
      </AnswerItemRoot>
    );
  }
}

AnswerItem.propTypes = {
  data: PropTypes.shape({
    title: PropTypes.string,
    profile: PropTypes.object.isRequired,
    content: PropTypes.string.isRequired,
    social: PropTypes.object.isRequired
  }).isRequired,
  expand: PropTypes.bool.isRequired,
  injectComponent: PropTypes.func.isRequired
};

export default AnswerItem;
