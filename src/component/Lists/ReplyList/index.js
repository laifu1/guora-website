import React, { Component } from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';
import Loading from 'widget/Loading';
import RestService from 'service/RequestServices/RestService';
import ReplyCard from 'component/TextCards/ReplyCard';
import { ShadowWhite, GrayLight4 } from 'style/Color';
import Time from 'util/Time';
import ReplyItem from './ReplyItem';
import More from '../More';

const Root = styled.div`
  position: relative;
`;

const Wrap = styled.div`
  display: block;
  padding: 16px;
  background-color: ${ShadowWhite};
  position: relative;
  min-height: 12px;
`;

const ThreadLine = styled.div`
  background-color: ${GrayLight4};
  position: absolute;
  bottom: 32px;
  width: 2px;
  top: 16px;
  left: 0;
`;

class ReplyList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      list: []
    };
    this.handleClickMore = this.handleClickMore.bind(this);
  }

  componentDidMount() {
    this.fetchData();
  }

  fetchData(offset = 0) {
    const { id } = this.props;
    const limit = 10;

    RestService.fetchRestList('replies', limit, offset, `commentID=${id}`)
      .then(message => {
        const list = message;
        this.state.loading = false;
        this.state.list = this.state.list.concat(list);
        this.setState(this.state);
      })
      .catch(err => {
        console.warn(err);
      });
  }

  fetchMore() {
    const { list } = this.state;
    const offset = list.length;
    this.fetchData(offset);
  }

  handleClickMore() {
    this.state.loading = true;
    this.fetchMore();
    this.setState(this.state);
  }

  render() {
    const { loading, list } = this.state;
    const { total } = this.props;

    const more = total - list.length;
    const hasMore = more > 0;

    return (
      <Root {...this.props}>
        <Wrap>
          <ThreadLine />
          {list.map(item => [
            <ReplyItem
              key={item.id}
              data={{
                id: item.id,
                content: item.content,
                profile: {
                  id: item.replyFromProfileID,
                  name: item.replyFromProfile.name,
                  desc: Time(item.createAt)
                },
                to: {
                  id: item.replyToProfileID,
                  name: item.replyToProfile.name
                }
              }}
              injectComponent={attr => (
                <ReplyCard
                  commentID={this.props.id}
                  to={{
                    id: item.replyFromProfileID,
                    name: item.replyFromProfile.name
                  }}
                  {...attr}
                />
              )}
            />
          ])}
          {!loading && hasMore && (
            <More onClick={this.handleClickMore}>
              <FormattedMessage id="list.reply.more" />({more})
            </More>
          )}
          {loading && <Loading />}
        </Wrap>
      </Root>
    );
  }
}

ReplyList.propTypes = {
  id: PropTypes.number.isRequired,
  total: PropTypes.number.isRequired
};

export default ReplyList;
