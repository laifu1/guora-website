import React from 'react';
import PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';
import styled from 'styled-components';
import ContainerWithBottomLine from 'widget/ContainerWithBottomLine';
import { Gray, GrayLight3 } from 'style/Color';
import LinkTitle from 'component/LinkTitle';
import { Bold } from 'style/Weight';
import { H4 } from 'widget/PageHead';
import { URL } from 'service/URLService';

const HotQuestionRoot = styled.div``;

const H3Container = styled(ContainerWithBottomLine)`
  padding-bottom: 8px;
  margin-bottom: 16px;
`;

const UL = styled.ul``;

const LI = styled.li`
  font-weight: ${Bold};
  margin-top: 8px;
`;

const GrayLinkTitle = styled(LinkTitle)`
  color: ${Gray};
  font-size: 14px;
`;

const HotQuestion = props => {
  const { data } = props;

  return (
    <HotQuestionRoot>
      <H3Container bottomLine>
        <H4 style={{ color: GrayLight3 }}>
          <FormattedMessage id="latest.questions" />
        </H4>
      </H3Container>
      <UL>
        {data.map(item => {
          return (
            <LI key={item.id}>
              <GrayLinkTitle href={URL.question(item.id)}>{item.title}</GrayLinkTitle>
            </LI>
          );
        })}
      </UL>
    </HotQuestionRoot>
  );
};

HotQuestion.propTypes = {
  data: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.number.isRequired,
      title: PropTypes.string.isRequired,
      type: PropTypes.number.isRequired
    })
  ).isRequired
};

export default HotQuestion;
