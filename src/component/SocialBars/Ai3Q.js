import styled from 'styled-components';

import { AiOutlineLoading3Quarters } from 'react-icons/ai';

const Ai3Q = styled(AiOutlineLoading3Quarters)`
  animation: spin 1s linear infinite;
`;

export default Ai3Q;
