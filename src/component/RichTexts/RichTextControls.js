/* eslint-disable import/prefer-default-export */
/* eslint-disable react/no-multi-comp */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { FaCode, FaQuoteLeft, FaLink, FaBold, FaItalic } from 'react-icons/fa';

export class StyleButton extends Component {
  constructor() {
    super();
    this.onToggle = e => {
      e.preventDefault();
      this.props.onToggle(this.props.style);
    };
  }

  render() {
    let className = 'rich-button';
    if (this.props.active) {
      className += ' rich-active-button';
    }
    if (this.props.disabled) {
      className += ' rich-disabled-button';
    }

    return (
      <span role="button" tabIndex="0" className={className} onMouseDown={this.onToggle}>
        {this.props.label}
      </span>
    );
  }
}

const BLOCK_TYPES = [
  // { label: 'H1', style: 'header-one' },
  // { label: 'H2', style: 'header-two' },
  // { label: 'H3', style: 'header-three' },
  // { label: 'H4', style: 'header-four' },
  // { label: 'H5', style: 'header-five' },
  // { label: 'H6', style: 'header-six' },
  { label: <FaQuoteLeft />, style: 'blockquote' },
  // { label: 'UL', style: 'unordered-list-item' },
  // { label: 'OL', style: 'ordered-list-item' },
  { label: <FaCode />, style: 'code-block' }
];

export const BlockStyleControls = props => {
  const { editorState } = props;
  const selection = editorState.getSelection();
  const blockType = editorState
    .getCurrentContent()
    .getBlockForKey(selection.getStartKey())
    .getType();

  return BLOCK_TYPES.map(type => (
    <StyleButton
      key={type.style}
      active={type.style === blockType}
      label={type.label}
      onToggle={props.onToggle}
      style={type.style}
    />
  ));
};

export const LinkStyleControl = props => {
  return <StyleButton active={false} label={<FaLink />} onToggle={props.onToggle} />;
};

const INLINE_STYLES = [
  { label: <FaBold />, style: 'BOLD' },
  { label: <FaItalic />, style: 'ITALIC' }
  // { label: 'Underline', style: 'UNDERLINE' },
  // { label: 'Monospace', style: 'CODE' }
];

export const InlineStyleControls = props => {
  const currentStyle = props.editorState.getCurrentInlineStyle();
  return INLINE_STYLES.map(type => (
    <StyleButton
      key={type.style}
      active={currentStyle.has(type.style)}
      label={type.label}
      onToggle={props.onToggle}
      style={type.style}
    />
  ));
};

StyleButton.defaultProps = {
  style: '',
  disabled: false
};

StyleButton.propTypes = {
  onToggle: PropTypes.func.isRequired,
  active: PropTypes.bool.isRequired,
  label: PropTypes.element.isRequired,
  style: PropTypes.string,
  disabled: PropTypes.bool
};

BlockStyleControls.propTypes = {
  // eslint-disable-next-line react/forbid-prop-types
  editorState: PropTypes.any.isRequired,
  onToggle: PropTypes.func.isRequired
};

LinkStyleControl.propTypes = {
  // eslint-disable-next-line react/forbid-prop-types
  // editorState: PropTypes.any.isRequired,
  onToggle: PropTypes.func.isRequired
};

InlineStyleControls.propTypes = {
  // eslint-disable-next-line react/forbid-prop-types
  editorState: PropTypes.any.isRequired,
  onToggle: PropTypes.func.isRequired
};
