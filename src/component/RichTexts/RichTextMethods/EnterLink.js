import {
  EditorState,
  RichUtils,
  CharacterMetadata,
  genKey,
  SelectionState,
  ContentBlock
} from 'draft-js';
import { List, Repeat } from 'immutable';

export default ({ editorState, linkText }) => {
  const selectionState = editorState.getSelection();
  const contentState = editorState.getCurrentContent();
  const currentBlock = contentState.getBlockForKey(selectionState.getStartKey());
  const currentBlockKey = currentBlock.getKey();
  const blockMap = contentState.getBlockMap();
  const blocksBefore = blockMap.toSeq().takeUntil(v => v === currentBlock);
  const blocksAfter = blockMap
    .toSeq()
    .skipUntil(v => v === currentBlock)
    .rest();
  const newBlockKey = genKey();

  // add new ContentBlock to editor state with appropriate text
  const newBlock = new ContentBlock({
    key: newBlockKey,
    type: 'unstyled',
    text: linkText,
    characterList: new List(Repeat(CharacterMetadata.create(), linkText.length))
  });

  const newBlockMap = blocksBefore
    .concat([[currentBlockKey, currentBlock], [newBlockKey, newBlock]], blocksAfter)
    .toOrderedMap();

  const selection = editorState.getSelection();

  const newContent = contentState.merge({
    blockMap: newBlockMap,
    selectionBefore: selection,
    selectionAfter: selection.merge({
      anchorKey: newBlockKey,
      anchorOffset: 0,
      focusKey: newBlockKey,
      focusOffset: 0,
      isBackward: false
    })
  });

  let newEditorState = EditorState.push(editorState, newContent, 'split-block');

  // programmatically apply selection on this text
  let newSelection = new SelectionState({
    anchorKey: newBlockKey,
    anchorOffset: 0,
    focusKey: newBlockKey,
    focusOffset: linkText.length
  });

  newEditorState = EditorState.forceSelection(newEditorState, newSelection);

  // create link entity
  const newContentState = newEditorState.getCurrentContent();

  const contentStateWithEntity = newContentState.createEntity('LINK', 'IMMUTABLE', {
    url: linkText
  });

  const entityKey = contentStateWithEntity.getLastCreatedEntityKey();
  newEditorState = EditorState.set(newEditorState, { currentContent: contentStateWithEntity });
  newEditorState = RichUtils.toggleLink(newEditorState, newEditorState.getSelection(), entityKey);

  // reset selection
  newSelection = new SelectionState({
    anchorKey: newBlockKey,
    anchorOffset: linkText.length,
    focusKey: newBlockKey,
    focusOffset: linkText.length
  });

  newEditorState = EditorState.forceSelection(newEditorState, newSelection);

  return newEditorState;
};
