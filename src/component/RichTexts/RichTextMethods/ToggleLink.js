import { EditorState, RichUtils } from 'draft-js';

export default ({ editorState, linkText }) => {
  const contentState = editorState.getCurrentContent();
  const contentStateWithEntity = contentState.createEntity('LINK', 'MUTABLE', { url: linkText });
  const entityKey = contentStateWithEntity.getLastCreatedEntityKey();
  let newEditorState = EditorState.set(editorState, { currentContent: contentStateWithEntity });
  newEditorState = RichUtils.toggleLink(newEditorState, newEditorState.getSelection(), entityKey);
  return newEditorState;
};
