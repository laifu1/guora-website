import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { White, Black, GrayLight1, GrayLight2, GrayLight4, StoneWhite } from 'style/Color';
import styled from 'styled-components';

const Form = styled.form`
  right: 50%;
  text-align: right;
  position: relative;
  margin-right: -150px;
`;

const LabelRoot = styled.div`
  padding: 5px;
`;

const Label = styled.label`
  display: inline-block;
  color: ${GrayLight1};
  line-height: 24px;
  font-size: 14px;
`;

const Input = styled.input`
  width: 240px;
  padding: 0 5px;
  color: ${Black};
  margin-left: 7px;
  font-size: inherit;
  border-radius: 2px;
  line-height: inherit;
  border: solid 1px ${GrayLight4};
  color: ${props => (props.disabled ? GrayLight2 : Black)};
  background-color: ${props => (props.disabled ? StoneWhite : White)};
`;

const Typer = {
  number: v => +v,
  string: v => v
};

const LabelInput = props => (
  <LabelRoot>
    <Label htmlFor={props.name}>
      {props.label}
      <Input
        type={props.type}
        onChange={e => props.onChange(Typer[props.type](e.target.value))}
        defaultValue={props.value}
        disabled={props.disabled}
      />
    </Label>
  </LabelRoot>
);

const HeadContainer = styled.div`
  padding: 5px 0;
`;

const FooterContainer = styled.div`
  display: flex;
  padding: 5px 0;
  justify-content: flex-end;
`;

LabelInput.defaultProps = {
  name: '',
  label: '',
  type: 'string',
  disabled: false,
  value: ''
};

LabelInput.propTypes = {
  name: PropTypes.string,
  label: PropTypes.string,
  type: PropTypes.string,
  disabled: PropTypes.bool,
  value: PropTypes.string || PropTypes.number,
  onChange: PropTypes.func.isRequired
};

class AdministratorBaseModal extends Component {
  constructor(props) {
    super(props);
    const form = {};
    Array.prototype.forEach.call(props.labels, item => {
      form[item.label] = item.value;
    });

    this.state = {
      form
    };
    this.handleChange = this.handleChange.bind(this);
  }

  getForm() {
    const { form } = this.state;
    return form;
  }

  handleChange(label, value) {
    const { form } = this.state;
    form[label] = value;
    this.setState({ form });
  }

  render() {
    const { labels, header, onSubmit, footer } = this.props;
    return [
      <HeadContainer key="header">{header}</HeadContainer>,
      <Form onSubmit={onSubmit} key="form">
        {labels.map(item => (
          <LabelInput
            key={item.label}
            label={item.label}
            name={item.label}
            type={item.type}
            disabled={item.disabled}
            value={item.value}
            onChange={value => this.handleChange(item.label, value)}
          />
        ))}
        <input type="submit" hidden />
      </Form>,
      <FooterContainer>{footer}</FooterContainer>
    ];
  }
}

AdministratorBaseModal.defaultProps = {
  labels: []
};

AdministratorBaseModal.propTypes = {
  labels: PropTypes.arrayOf(
    PropTypes.shape({
      label: PropTypes.string,
      disabled: PropTypes.bool,
      type: PropTypes.string,
      value: PropTypes.string || PropTypes.number
    })
  ).isRequired,
  header: PropTypes.node.isRequired,
  onSubmit: PropTypes.func.isRequired,
  footer: PropTypes.node.isRequired
};

export default AdministratorBaseModal;
