import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';
import { White, Black, GrayLight1, GrayLight4, Blue, SkyBlue } from 'style/Color';
import Range from 'util/Range';
import styled from 'styled-components';

const Root = styled.div`
  box-sizing: border-box;
  position: relative;
  overflow: hidden;
  display: flex;
  height: 100%;
`;

const Wrap = styled.div`
  border: 1px solid ${props => (props.active ? SkyBlue : GrayLight4)};
  box-sizing: border-box;
  border-radius: 3px;
  position: relative;
  margin-right: 5px;
  overflow: hidden;
  display: flex;
  height: 100%;
`;

const Select = styled.select`
  flex: 1;
  margin: 2px;
  border: none;
  font-size: 12px;
  position: relative;
  color: ${Black};
  box-sizing: border-box;
  background-color: transparent;
`;

const Option = styled.option``;

const Button = styled.button`
  flex: 1;
  border: none;
  font-size: 12px;
  cursor: pointer;
  border-radius: 2px;
  color: ${props => (props.active ? Blue : GrayLight1)};
`;

const Submit = styled(Button)`
  background-color: ${White};
`;

const Reset = styled(Button)`
  background-color: ${props => (props.active ? SkyBlue : White)};
`;

class Pagination extends Component {
  constructor(props) {
    super(props);
    this.state = {
      p: props.pageNum,
      resetable: props.pageNum > 1,
      isDirty: false
    };
    this.handleSubmit = () => {
      const { p } = this.state;
      this.props.onPagination(p);
    };
    this.handleSelect = event => {
      const { pageNum } = this.props;
      const isDirty = +event.target.value !== pageNum;
      this.setState({ p: event.target.value, isDirty });
    };
  }

  render() {
    const { isDirty, resetable } = this.state;
    const { pageTotal, pageNum, lock } = this.props;

    if (lock) {
      return (
        <Root>
          <Wrap active="true">
            <Reset onClick={this.props.onReset} active="true">
              <FormattedMessage id="reset" />
            </Reset>
          </Wrap>
        </Root>
      );
    }

    return (
      <Root>
        <Wrap>
          <Select onChange={this.handleSelect}>
            {Range(1, pageTotal).map(num => (
              <FormattedMessage key={num} id="admin.pagination">
                {([txt]) => (
                  <Option key={num} value={num} selected={num === pageNum}>
                    {txt}.{num}
                  </Option>
                )}
              </FormattedMessage>
            ))}
          </Select>
          <Submit onClick={this.handleSubmit} active={isDirty}>
            <FormattedMessage id="submit2" />
          </Submit>
        </Wrap>
        <Wrap active={resetable}>
          <Reset onClick={this.props.onReset} active={resetable}>
            <FormattedMessage id="reset" />
          </Reset>
        </Wrap>
      </Root>
    );
  }
}

Pagination.defaultProps = {
  lock: false
};

Pagination.propTypes = {
  onPagination: PropTypes.func.isRequired,
  onReset: PropTypes.func.isRequired,
  pageNum: PropTypes.number.isRequired,
  lock: PropTypes.bool,
  pageTotal: PropTypes.number.isRequired
};

export default Pagination;
