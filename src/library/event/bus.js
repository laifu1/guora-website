class EventBus {
  constructor() {
    this.bus = {};
  }

  $on(id, callback) {
    this.bus[id] = callback;
  }

  $emit(id, ...variaveis) {
    this.bus[id](...variaveis);
  }
}

const bus = new EventBus();
export default bus;
