import React from 'react';
import PropTypes from 'prop-types';
import ProfileCard from 'component/ProfileCard';
import ProfileTab from 'component/ProfileTab';
import QuestionList from 'component/Lists/QuestionList';
import AnswerList from 'component/Lists/AnswerList';
import DocumentList from 'component/Lists/DocumentList';
import Card from 'widget/Card';
import styled from 'styled-components';
import MeService from 'service/MeService';
import HotQuestion from 'component/HotQuestion';
import { DashboardRoot, MainColumnRoot, SideColumnRoot } from 'style/DashboardColumnStyled';
import { URL } from 'service/URLService';

const CSR = window.$CSRDATA;
const CSRProfile = CSR.profile;
const CSRQuestions = CSR.questions;
const CSRQuestionsCounts = CSR.questionsCounts;
const CSRAnswers = CSR.answers;
const CSRAnswersCounts = CSR.answersCounts;
const CSRHotQuestions = CSR.hotQuestions;

const ProfileFeedsCard = styled(Card)`
  margin-top: 10px;
`;

const ID = +URL.search('id');
const card = URL.search('card');

const isMe = MeService.GetMe().id === ID;

let focus;

switch (card) {
  case 'question': {
    focus = 1;
    break;
  }
  case 'document': {
    focus = 2;
    break;
  }
  default: {
    focus = 0;
  }
}

const Select = index => {
  URL.go(
    [`${URL.profile(ID)}`, `${URL.profile(ID)}&card=question`, `${URL.profile(ID)}&card=document`][
      index
    ]
  );
};

const Page = props => {
  return (
    <DashboardRoot>
      <MainColumnRoot>
        <ProfileCard data={CSRProfile} isMe={isMe} onClickUpload={props.onClickUpload} />
        <ProfileFeedsCard>
          <ProfileTab
            data={{ answersCounts: CSRAnswersCounts, questionsCounts: CSRQuestionsCounts }}
            focus={focus}
            onSelect={Select}
          />
          {focus === 0 && (
            <AnswerList
              from="profile"
              id={ID}
              data={{ list: CSRAnswers, total: CSRAnswersCounts }}
            />
          )}
          {focus === 1 && (
            <QuestionList id={ID} data={{ list: CSRQuestions, total: CSRQuestionsCounts }} />
          )}
          {focus === 2 && <DocumentList />}
        </ProfileFeedsCard>
      </MainColumnRoot>
      <SideColumnRoot>
        <HotQuestion data={CSRHotQuestions} />
      </SideColumnRoot>
    </DashboardRoot>
  );
};

Page.propTypes = {
  onClickUpload: PropTypes.func.isRequired
};

export default Page;
