import React, { Component } from 'react';
import styled from 'styled-components';

import BasePageCombination from 'component/Combinations/PageCombination/BasePageCombination';
import { ContentColumn } from 'style/Layout';
import UploadModal from 'component/Modals/UploadModal';
import FileService from 'service/RequestServices/FileService';
import MeService from 'service/MeService';
import { URL } from 'service/URLService';

import Page from './Page';

const AppCenterRoot = styled.div`
  width: ${ContentColumn};
  margin: auto;
`;

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      modal: false
    };
    this.toggle = () => {
      let { modal } = this.state;
      modal = !modal;
      this.setState({ modal });
    };
    this.upload = blob => {
      const form = new FormData();
      form.append('avatar', blob);
      FileService.uploadAvatar(form).then(() => {
        URL.go(`${URL.profile(MeService.GetMe().id)}&ts=${new Date().getTime()}`);
      });
    };
  }

  render() {
    const { modal } = this.state;
    return [
      <BasePageCombination key="combination" />,
      modal ? <UploadModal key="modal" onClose={this.toggle} onSubmit={this.upload} /> : null,
      <AppCenterRoot key="app">
        <Page onClickUpload={this.toggle} />
      </AppCenterRoot>
    ];
  }
}

export default App;
